package uk.co.cablepost.better_crossbows.item;

import com.google.common.collect.Lists;
import net.minecraft.advancement.criterion.Criteria;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.client.item.TooltipData;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtList;
import net.minecraft.screen.slot.Slot;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.stat.Stats;
import net.minecraft.tag.ItemTags;
import net.minecraft.text.Text;
import net.minecraft.util.*;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.better_crossbows.client.tooltip.BetterCrossbowItemTooltipData;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class BetterCrossbowItem extends CrossbowItem {
    private static final String INVENTORY_KEY = "Inventory";
    private static final String CHARGED_PROJECTILES_KEY = "ChargedProjectiles";

    public int maxInventoryItemCount;
    public int stringCount;

    public BetterCrossbowItem(Settings settings, int maxInventoryItemCount, int stringCount) {
        super(settings);
        this.maxInventoryItemCount = maxInventoryItemCount;
        this.stringCount = stringCount;
    }

    @Override
    public Predicate<ItemStack> getHeldProjectiles() {
        return CROSSBOW_HELD_PROJECTILES;
    }

    @Override
    public Predicate<ItemStack> getProjectiles() {
        return BOW_PROJECTILES;
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack crossbowItemStack = user.getStackInHand(hand);
        if (CrossbowItem.isCharged(crossbowItemStack)) {
            //CrossbowItem.shootAll(world, user, hand, crossbowItemStack, CrossbowItem.getSpeed(crossbowItemStack), 1.0f);
            betterCrossbowShoot(crossbowItemStack, world, user, hand);
            return TypedActionResult.consume(crossbowItemStack);
        }
        if (!getAmmoToLoad(crossbowItemStack).isEmpty()) {
            if (!CrossbowItem.isCharged(crossbowItemStack)) {
                user.setCurrentHand(hand);
            }
            return TypedActionResult.consume(crossbowItemStack);
        }
        return TypedActionResult.fail(crossbowItemStack);
    }

    private static float getProjectileSpeed(ItemStack projectile){
        if (projectile.isOf(Items.FIREWORK_ROCKET)) {
            return 1.6f;
        }
        return 3.15f;
    }

    private static void betterCrossbowShoot(ItemStack crossbowItemStack, World world, LivingEntity entity, Hand hand){
        List<ItemStack> list = CrossbowItem.getProjectiles(crossbowItemStack);
        ItemStack itemStack = list.get(0);

        if (!itemStack.isEmpty()) {
            float[] fs = CrossbowItem.getSoundPitches(entity.getRandom());
            float speed = getProjectileSpeed(itemStack);

            int multiShotEnchantLevel = EnchantmentHelper.getLevel(Enchantments.MULTISHOT, crossbowItemStack);
            int maxAmountToShootPerShot = multiShotEnchantLevel == 0 ? 1 : 3;

            for (int i = 0; i < maxAmountToShootPerShot; i++) {
                if (i == 0) {
                    CrossbowItem.shoot(world, entity, hand, crossbowItemStack, itemStack, fs[i], false, speed, 1.0f, 0.0f);
                    continue;
                }
                if (i == 1) {
                    CrossbowItem.shoot(world, entity, hand, crossbowItemStack, itemStack, fs[i], false, speed, 1.0f, -10.0f);
                    continue;
                }
                CrossbowItem.shoot(world, entity, hand, crossbowItemStack, itemStack, fs[i], false, speed, 1.0f, 10.0f);
            }
        }

        betterCrossbowPostShoot(world, entity, crossbowItemStack);
    }

    private static void betterCrossbowPostShoot(World world, LivingEntity entity, ItemStack crossbowStack) {
        if (entity instanceof ServerPlayerEntity serverPlayerEntity) {
            if (!world.isClient) {
                Criteria.SHOT_CROSSBOW.trigger(serverPlayerEntity, crossbowStack);
            }
            serverPlayerEntity.incrementStat(Stats.USED.getOrCreateStat(crossbowStack.getItem()));
        }

        removeFirstStack(crossbowStack, CHARGED_PROJECTILES_KEY);

        if (CrossbowItem.getProjectiles(crossbowStack).size() == 0) {
            CrossbowItem.setCharged(crossbowStack, false);
        }
    }

    @Override
    public void onStoppedUsing(ItemStack crossbowItemStack, World world, LivingEntity user, int remainingUseTicks) {
        int i = this.getMaxUseTime(crossbowItemStack) - remainingUseTicks;
        float f = net.minecraft.item.CrossbowItem.getPullProgress(i, crossbowItemStack);
        if (f >= 1.0f && !net.minecraft.item.CrossbowItem.isCharged(crossbowItemStack) && loadProjectiles(crossbowItemStack)) {
            net.minecraft.item.CrossbowItem.setCharged(crossbowItemStack, true);
            SoundCategory soundCategory = user instanceof PlayerEntity ? SoundCategory.PLAYERS : SoundCategory.HOSTILE;
            world.playSound(null, user.getX(), user.getY(), user.getZ(), SoundEvents.ITEM_CROSSBOW_LOADING_END, soundCategory, 1.0f, 1.0f / (world.getRandom().nextFloat() * 0.5f + 1.0f) + 0.2f);
        }
    }

    private ItemStack getAmmoToLoad(ItemStack crossbowItemStack){
        List<ItemStack> inventory = getInventory(crossbowItemStack);

        for(ItemStack itemStack : inventory){
            if(!itemStack.isEmpty()){
                return itemStack;
            }
        }

        return ItemStack.EMPTY;
    }

    private boolean loadProjectiles(ItemStack crossbowItemStack) {
        boolean toRet = false;

        for (int i = 0; i < stringCount; i++) {
            ItemStack ammoToLoad = getAmmoToLoad(crossbowItemStack);

            if(ammoToLoad.isEmpty()){
                continue;
            }

            toRet = true;//Loaded at least one

            ItemStack ammoToLoadSingle = ammoToLoad.copy();
            ammoToLoadSingle.setCount(1);
            net.minecraft.item.CrossbowItem.putProjectile(crossbowItemStack, ammoToLoadSingle);

            ammoToLoad.decrement(1);
            updateInventoryStack(crossbowItemStack, 0, ammoToLoad);
        }

        return toRet;
    }

    @Override
    public void usageTick(World world, LivingEntity user, ItemStack crossbowStack, int remainingUseTicks) {
        if (!world.isClient) {
            int i = EnchantmentHelper.getLevel(Enchantments.QUICK_CHARGE, crossbowStack);
            SoundEvent soundEventQuickCharge = getQuickChargeSound(i);
            SoundEvent soundEventLoadingMiddle = i == 0 ? SoundEvents.ITEM_CROSSBOW_LOADING_MIDDLE : null;
            float f = (float)(crossbowStack.getMaxUseTime() - remainingUseTicks) / (float) net.minecraft.item.CrossbowItem.getPullTime(crossbowStack);

            if (f >= 0.2f && f < 1.0f && !CrossbowItem.isCharged(crossbowStack)) {
                world.playSound(null, user.getX(), user.getY(), user.getZ(), soundEventQuickCharge, SoundCategory.PLAYERS, 0.5f, 1.0f);
            }
            if (f >= 0.5f && f < 1.0f && soundEventLoadingMiddle != null && CrossbowItem.getProjectiles(crossbowStack).size() == 0) {
                world.playSound(null, user.getX(), user.getY(), user.getZ(), soundEventLoadingMiddle, SoundCategory.PLAYERS, 0.5f, 1.0f);
            }
        }
    }

    private SoundEvent getQuickChargeSound(int stage) {
        switch (stage) {
            case 1 -> {
                return SoundEvents.ITEM_CROSSBOW_QUICK_CHARGE_1;
            }
            case 2 -> {
                return SoundEvents.ITEM_CROSSBOW_QUICK_CHARGE_2;
            }
            case 3 -> {
                return SoundEvents.ITEM_CROSSBOW_QUICK_CHARGE_3;
            }
        }
        return SoundEvents.ITEM_CROSSBOW_LOADING_START;
    }

    @Override
    public int getMaxUseTime(ItemStack stack) {
        return net.minecraft.item.CrossbowItem.getPullTime(stack) + 3;
    }

    @Override
    public UseAction getUseAction(ItemStack stack) {
        return UseAction.CROSSBOW;
    }

    @Override
    public void appendTooltip(ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context) {
//        List<ItemStack> list = CrossbowItem.getProjectiles(stack);
//        if (!CrossbowItem.isCharged(stack) || list.isEmpty()) {
//            return;
//        }
//        ItemStack itemStack = list.get(0);
////        tooltip.add(new TranslatableText("item.minecraft.crossbow.projectile").append(" ").append(itemStack.toHoverableText()));
//        if (context.isAdvanced() && itemStack.isOf(Items.FIREWORK_ROCKET)) {
//            ArrayList<Text> list2 = Lists.newArrayList();
//            Items.FIREWORK_ROCKET.appendTooltip(itemStack, world, list2, context);
//            if (!list2.isEmpty()) {
//                list2.replaceAll(text -> new LiteralText("  ").append(text).formatted(Formatting.GRAY));
////                tooltip.addAll(list2);
//            }
//        }
    }

    @Override
    public Optional<TooltipData> getTooltipData(ItemStack stack) {
        List<ItemStack> loaded = net.minecraft.item.CrossbowItem.getProjectiles(stack);
        List<ItemStack> inventory = getInventory(stack);
        return Optional.of(new BetterCrossbowItemTooltipData(inventory, loaded, maxInventoryItemCount));
    }

    @Override
    public boolean isUsedOnRelease(ItemStack stack) {
        return stack.isOf(this);
    }

    @Override
    public int getRange() {
        return 8;
    }

    private static boolean validCrossbowAmmo(ItemStack ammo){
        return
                ammo.isIn(ItemTags.ARROWS) ||
                ammo.getItem() == Items.FIREWORK_ROCKET
        ;
    }

    private static List<ItemStack> getInventory(ItemStack crossbow) {
        NbtList nbtList;
        ArrayList<ItemStack> list = Lists.newArrayList();
        NbtCompound nbtCompound = crossbow.getNbt();
        if (nbtCompound != null && nbtCompound.contains(INVENTORY_KEY, 9) && (nbtList = nbtCompound.getList(INVENTORY_KEY, 10)) != null) {
            for (int i = 0; i < nbtList.size(); ++i) {
                NbtCompound nbtCompound2 = nbtList.getCompound(i);
                list.add(ItemStack.fromNbt(nbtCompound2));
            }
        }
        return list;
    }

    private static int getInventoryItemCount(ItemStack crossbow) {
        int toRet = 0;

        for(ItemStack itemStack : getInventory(crossbow)){
            toRet += itemStack.getCount();
        }
        return toRet;
    }

    private int addToInventory(ItemStack crossbow, ItemStack ammo) {
        if (ammo.isEmpty() || !validCrossbowAmmo(ammo)) {
            return 0;
        }

        NbtCompound nbtCompound = crossbow.getOrCreateNbt();
        if (!nbtCompound.contains(INVENTORY_KEY)) {
            nbtCompound.put(INVENTORY_KEY, new NbtList());
        }

        int inventoryItemCount = getInventoryItemCount(crossbow);
        int amountToInsert = Math.min(ammo.getCount(), (maxInventoryItemCount - inventoryItemCount));
        if (amountToInsert == 0) {
            return 0;
        }

        NbtList nbtList = nbtCompound.getList(INVENTORY_KEY, 10);

        ItemStack itemStack2 = ammo.copy();
        itemStack2.setCount(amountToInsert);
        NbtCompound nbtCompound3 = new NbtCompound();
        itemStack2.writeNbt(nbtCompound3);
        nbtList.add(0, nbtCompound3);

        ammo.decrement(amountToInsert);

        return amountToInsert;
    }

    private static Optional<ItemStack> removeFirstStack(ItemStack crossbowStack, String key) {
        NbtCompound nbtCompound = crossbowStack.getOrCreateNbt();
        if (!nbtCompound.contains(key)) {
            return Optional.empty();
        }
        NbtList nbtList = nbtCompound.getList(key, 10);
        if (nbtList.isEmpty()) {
            return Optional.empty();
        }
        NbtCompound nbtCompound2 = nbtList.getCompound(0);
        ItemStack itemStack = ItemStack.fromNbt(nbtCompound2);
        nbtList.remove(0);
        if (nbtList.isEmpty()) {
            crossbowStack.removeSubNbt(key);
        }
        return Optional.of(itemStack);
    }

    private static Optional<ItemStack> updateInventoryStack(ItemStack crossbowStack, int slot, ItemStack newItemStack) {
        NbtCompound crossbowNbtCompound = crossbowStack.getOrCreateNbt();
        if (!crossbowNbtCompound.contains(INVENTORY_KEY)) {
            return Optional.empty();
        }
        NbtList nbtList = crossbowNbtCompound.getList(INVENTORY_KEY, 10);
        if (nbtList.isEmpty()) {
            return Optional.empty();
        }
        NbtCompound nbtCompoundOfInvStackInSlot = nbtList.getCompound(slot);
        ItemStack originalItemStack = ItemStack.fromNbt(nbtCompoundOfInvStackInSlot);

        if(newItemStack.isEmpty()) {
            nbtList.remove(slot);
        }
        else {
            NbtCompound nbtCompoundToUpdate = new NbtCompound();
            newItemStack.writeNbt(nbtCompoundToUpdate);

            nbtList.set(slot, nbtCompoundToUpdate);
        }

        if (nbtList.isEmpty()) {
            crossbowStack.removeSubNbt(INVENTORY_KEY);
        }
        return Optional.of(originalItemStack);
    }

    @Override
    public boolean onStackClicked(ItemStack crossbowStack, Slot slot, ClickType clickType, PlayerEntity player) {
        if (clickType != ClickType.RIGHT) {
            return false;
        }
        ItemStack itemStack = slot.getStack();
        if (itemStack.isEmpty()) {
            removeFirstStack(crossbowStack, INVENTORY_KEY).ifPresent(removedStack -> {
                addToInventory(crossbowStack, slot.insertStack(removedStack));
                this.playRemoveOneSound(player);
            });
        } else {
            int addedCount = addToInventory(crossbowStack, slot.getStack());
            if (addedCount > 0) {
                this.playInsertSound(player);
            }
        }
        return true;
    }

    private void playRemoveOneSound(Entity entity) {
        entity.playSound(SoundEvents.ITEM_BUNDLE_REMOVE_ONE, 0.8f, 0.8f + entity.getWorld().getRandom().nextFloat() * 0.4f);
    }

    private void playInsertSound(Entity entity) {
        entity.playSound(SoundEvents.ITEM_BUNDLE_INSERT, 0.8f, 0.8f + entity.getWorld().getRandom().nextFloat() * 0.4f);
    }
}
