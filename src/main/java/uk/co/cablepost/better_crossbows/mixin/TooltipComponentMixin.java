package uk.co.cablepost.better_crossbows.mixin;

import net.minecraft.client.gui.tooltip.TooltipComponent;
import net.minecraft.client.item.TooltipData;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import uk.co.cablepost.better_crossbows.client.tooltip.BetterCrossbowItemTooltipComponent;
import uk.co.cablepost.better_crossbows.client.tooltip.BetterCrossbowItemTooltipData;

@Mixin(TooltipComponent.class)
public interface TooltipComponentMixin {
    @Inject(method = "of(Lnet/minecraft/client/item/TooltipData;)Lnet/minecraft/client/gui/tooltip/TooltipComponent;", at = @At("HEAD"), cancellable = true)
    private static void isOf(TooltipData data, CallbackInfoReturnable<TooltipComponent> cir) {
        if (data instanceof BetterCrossbowItemTooltipData) {
            cir.setReturnValue(new BetterCrossbowItemTooltipComponent((BetterCrossbowItemTooltipData)data));
        }
    }
}
