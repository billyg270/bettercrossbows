package uk.co.cablepost.better_crossbows.mixin;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import uk.co.cablepost.better_crossbows.item.BetterCrossbowItem;

@Mixin(ItemStack.class)
public abstract class ItemStackMixin {
    @Shadow public abstract Item getItem();

    @Inject(method = "isOf(Lnet/minecraft/item/Item;)Z", at = @At("HEAD"), cancellable = true)
    public void isOf(Item item, CallbackInfoReturnable<Boolean> cir) {
        if(item instanceof net.minecraft.item.CrossbowItem && this.getItem() instanceof BetterCrossbowItem){
            cir.setReturnValue(true);
        }
    }
}
