package uk.co.cablepost.better_crossbows;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import uk.co.cablepost.better_crossbows.item.BetterCrossbowItem;

import java.util.Arrays;
import java.util.List;

public class BetterCrossbows implements ModInitializer {

    public static final String MOD_ID = "better_crossbows";

    public static class BetterCrossbowItemAndMeta {
        public Identifier identifier;
        public BetterCrossbowItem item;

        public BetterCrossbowItemAndMeta(String idStr, BetterCrossbowItem item){
            this.identifier = new Identifier(BetterCrossbows.MOD_ID, idStr);
            this.item = item;
        }
    }

    public static final List<BetterCrossbowItemAndMeta> CROSSBOW_ITEMS = Arrays.asList(
            new BetterCrossbowItemAndMeta(
                "iron_crossbow",
                new BetterCrossbowItem(
                        new FabricItemSettings()
                                .group(ItemGroup.COMBAT)
                                .maxCount(1)
                                .maxDamage(500),
                        64 * 3,
                        1
                )
            ),
            new BetterCrossbowItemAndMeta(
                    "gold_crossbow",
                    new BetterCrossbowItem(
                            new FabricItemSettings()
                                    .group(ItemGroup.COMBAT)
                                    .maxCount(1)
                                    .maxDamage(60),
                            32,
                            5
                    )
            ),
            new BetterCrossbowItemAndMeta(
                    "netherite_crossbow",
                    new BetterCrossbowItem(
                            new FabricItemSettings()
                                    .group(ItemGroup.COMBAT)
                                    .maxCount(1)
                                    .maxDamage(2000),
                            64 * 16,
                            10
                    )
            )
    );

    @Override
    public void onInitialize() {
        for(BetterCrossbowItemAndMeta crossbowItemAndMeta : CROSSBOW_ITEMS){
            Registry.register(Registry.ITEM, crossbowItemAndMeta.identifier, crossbowItemAndMeta.item);
        }
    }
}
