package uk.co.cablepost.better_crossbows.client.tooltip;

import net.minecraft.client.item.TooltipData;
import net.minecraft.item.ItemStack;

import java.util.List;

public class BetterCrossbowItemTooltipData implements TooltipData {
    private final List<ItemStack> inventory;
    private final int maxInventoryItemCount;
    private final List<ItemStack> loaded;

    public BetterCrossbowItemTooltipData(List<ItemStack> inventory, List<ItemStack> loaded, int maxInventoryItemCount) {
        this.inventory = inventory;
        this.maxInventoryItemCount = maxInventoryItemCount;
        this.loaded = loaded;
    }

    public List<ItemStack> getInventory() {
        return this.inventory;
    }

    public List<ItemStack> getLoaded() {
        return this.loaded;
    }

    public int getMaxInventoryItemCount() {
        return this.maxInventoryItemCount;
    }

    public int getInventoryItemCount() {
        int toRet = 0;

        for(ItemStack itemStack : inventory){
            toRet += itemStack.getCount();
        }
        return toRet;
    }
}
