package uk.co.cablepost.better_crossbows.client.tooltip;

import com.mojang.blaze3d.systems.RenderSystem;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.gui.tooltip.TooltipComponent;
import net.minecraft.client.render.LightmapTextureManager;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.item.ItemRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.Matrix4f;

import java.util.List;

public class BetterCrossbowItemTooltipComponent implements TooltipComponent {
    public static final Identifier TEXTURE = new Identifier("textures/gui/container/bundle.png");
    private final List<ItemStack> inventory;
    private final int inventoryItemCount;
    private final int maxInventoryItemCount;
    private final List<ItemStack> loaded;

    public BetterCrossbowItemTooltipComponent(BetterCrossbowItemTooltipData data) {
        this.inventory = data.getInventory();
        this.inventoryItemCount = data.getInventoryItemCount();
        this.maxInventoryItemCount = data.getMaxInventoryItemCount();
        this.loaded = data.getLoaded();
    }

    @Override
    public int getHeight() {
        return Math.max(this.getInventoryRows(), 1) * 20 + 2 + 4 + 50;
    }

    @Override
    public int getWidth(TextRenderer textRenderer) {
        return Math.max(this.getInventoryColumns() * 18 + 2, 270);
    }

    @Override
    public void drawItems(TextRenderer textRenderer, int x, int y, MatrixStack matrices, ItemRenderer itemRenderer, int z) {
        //inventory
        int inventoryColumns = this.getInventoryColumns();
        int inventoryRows = this.getInventoryRows();
        if(inventory.size() > 0) {
            int invIndex = 0;
            for (int invRow = 0; invRow < inventoryRows; ++invRow) {
                for (int invCol = 0; invCol < inventoryColumns; ++invCol) {
                    int slotX = x + invCol * 18 + 1;
                    int slotY = y + invRow * 20 + 1 + 10;
                    ItemStack itemStack;
                    try {
                        itemStack = inventory.get(invIndex);
                    } catch (Exception exception) {
                        itemStack = ItemStack.EMPTY;
                    }
                    this.drawSlot(slotX, slotY, itemStack, textRenderer, matrices, itemRenderer, z);
                    invIndex++;
                }
            }
            this.drawSlotsOutline(x, y + 10, inventoryColumns, inventoryRows, matrices, z);
        }

        //loaded
        if(loaded.size() > 0) {
            this.drawSlotsOutline(x, y + Math.max(inventoryRows, 1) * 20 + 26, loaded.size(), 1, matrices, z);
            for (int loadedIndex = 0; loadedIndex < loaded.size(); loadedIndex++) {
                this.drawSlot(x + loadedIndex * 18 + 1, y + Math.max(inventoryRows, 1) * 20 + 26 + 1, loaded.get(loadedIndex), textRenderer, matrices, itemRenderer, z);
            }
        }
    }

    @Override
    public void drawText(TextRenderer textRenderer, int x, int y, Matrix4f matrix, VertexConsumerProvider.Immediate vertexConsumers) {
        //inventory
        int inventoryRows = this.getInventoryRows();

        if(inventory.size() > 0) {
            textRenderer.draw(Text.of("Ammo in inventory (" + inventoryItemCount + "/"  + maxInventoryItemCount + "):"), (float)x, (float)(y), 0xFFFFFF, false, matrix, vertexConsumers, false, 0, LightmapTextureManager.MAX_LIGHT_COORDINATE);
        }else{
            textRenderer.draw(Text.of("Nothing in inventory, use like a bundle to add ammo"), (float)x, (float)(y + 6), 0xbdbdbd, false, matrix, vertexConsumers, false, 0, LightmapTextureManager.MAX_LIGHT_COORDINATE);
        }

        //loaded
        int loadedTextY = y + Math.max(inventoryRows, 1) * 20 + 6 + 10;
        if(loaded.size() > 0) {
            textRenderer.draw(Text.of("Loaded:"), (float)x, (float) loadedTextY, 0xFFFFFF, false, matrix, vertexConsumers, false, 0, LightmapTextureManager.MAX_LIGHT_COORDINATE);
        }
        else{
            textRenderer.draw(Text.of("Nothing loaded, hold right click once ammo is added"), (float)x, (float) loadedTextY, 0xbdbdbd, false, matrix, vertexConsumers, false, 0, LightmapTextureManager.MAX_LIGHT_COORDINATE);
        }
    }

    private void drawSlot(
        int x,
        int y,
        ItemStack itemStack,
        TextRenderer textRenderer,
        net.minecraft.client.util.math.MatrixStack matrices,
        ItemRenderer itemRenderer,
        int z
    ) {
//        if (index >= this.inventory.size()) {
//            this.drawSprite(matrices, x, y, z, shouldBlock ? Sprite.BLOCKED_SLOT : Sprite.SLOT);
//            return;
//        }
        //ItemStack itemStack = this.inventory.get(index);
        this.drawSprite(matrices, x, y, z, Sprite.SLOT);
        itemRenderer.renderInGuiWithOverrides(itemStack, x + 1, y + 1, 0);
        itemRenderer.renderGuiItemOverlay(textRenderer, itemStack, x + 1, y + 1);
        //if (index == 0) {
        //    HandledScreen.drawSlotHighlight(matrices, x + 1, y + 1, z);
        //}
    }

    private void drawSlotsOutline(int x, int y, int columns, int rows, MatrixStack matrices, int z) {
        int i;
        this.drawSprite(matrices, x, y, z, Sprite.BORDER_CORNER_TOP);
        this.drawSprite(matrices, x + columns * 18 + 1, y, z, Sprite.BORDER_CORNER_TOP);
        for (i = 0; i < columns; ++i) {
            this.drawSprite(matrices, x + 1 + i * 18, y, z, Sprite.BORDER_HORIZONTAL_TOP);
            this.drawSprite(matrices, x + 1 + i * 18, y + rows * 20, z, Sprite.BORDER_HORIZONTAL_BOTTOM);
        }
        for (i = 0; i < rows; ++i) {
            this.drawSprite(matrices, x, y + i * 20 + 1, z, Sprite.BORDER_VERTICAL);
            this.drawSprite(matrices, x + columns * 18 + 1, y + i * 20 + 1, z, Sprite.BORDER_VERTICAL);
        }
        this.drawSprite(matrices, x, y + rows * 20, z, Sprite.BORDER_CORNER_BOTTOM);
        this.drawSprite(matrices, x + columns * 18 + 1, y + rows * 20, z, Sprite.BORDER_CORNER_BOTTOM);
    }

    private void drawSprite(net.minecraft.client.util.math.MatrixStack matrices, int x, int y, int z, Sprite sprite) {
        RenderSystem.setShaderColor(1.0f, 1.0f, 1.0f, 1.0f);
        RenderSystem.setShaderTexture(0, TEXTURE);
        DrawableHelper.drawTexture(matrices, x, y, z, sprite.u, sprite.v, sprite.width, sprite.height, 128, 128);
    }

    private int getInventoryColumns() {
        return (int)Math.ceil(Math.sqrt(this.inventory.size()));
    }

    private int getInventoryRows() {
        return (int)Math.ceil(((double)this.inventory.size()) / (double)this.getInventoryColumns());
    }

    @Environment(value= EnvType.CLIENT)
    enum Sprite {
        SLOT(0, 0, 18, 20),
        BLOCKED_SLOT(0, 40, 18, 20),
        BORDER_VERTICAL(0, 18, 1, 20),
        BORDER_HORIZONTAL_TOP(0, 20, 18, 1),
        BORDER_HORIZONTAL_BOTTOM(0, 60, 18, 1),
        BORDER_CORNER_TOP(0, 20, 1, 1),
        BORDER_CORNER_BOTTOM(0, 60, 1, 1);

        public final int u;
        public final int v;
        public final int width;
        public final int height;

        Sprite(int u, int v, int width, int height) {
            this.u = u;
            this.v = v;
            this.width = width;
            this.height = height;
        }
    }
}
