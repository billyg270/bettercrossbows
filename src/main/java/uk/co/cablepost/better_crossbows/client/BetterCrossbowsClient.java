package uk.co.cablepost.better_crossbows.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.item.ModelPredicateProviderRegistry;
import net.minecraft.item.CrossbowItem;
import net.minecraft.item.Items;
import net.minecraft.util.Identifier;
import uk.co.cablepost.better_crossbows.BetterCrossbows;

@Environment(EnvType.CLIENT)
public class BetterCrossbowsClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        for(BetterCrossbows.BetterCrossbowItemAndMeta crossbowItemAndMeta : BetterCrossbows.CROSSBOW_ITEMS){
            ModelPredicateProviderRegistry.register(crossbowItemAndMeta.item, new Identifier("pull"), (stack, world, entity, seed) -> {
                if (entity == null) {
                    return 0.0f;
                }
                if (CrossbowItem.isCharged(stack)) {
                    return 0.0f;
                }
                return (float)(stack.getMaxUseTime() - entity.getItemUseTimeLeft()) / (float)CrossbowItem.getPullTime(stack);
            });
            ModelPredicateProviderRegistry.register(crossbowItemAndMeta.item, new Identifier("pulling"), (stack, world, entity, seed) -> entity != null && entity.isUsingItem() && entity.getActiveItem() == stack && !CrossbowItem.isCharged(stack) ? 1.0f : 0.0f);
            ModelPredicateProviderRegistry.register(crossbowItemAndMeta.item, new Identifier("charged"), (stack, world, entity, seed) -> entity != null && CrossbowItem.isCharged(stack) ? 1.0f : 0.0f);
            ModelPredicateProviderRegistry.register(crossbowItemAndMeta.item, new Identifier("firework"), (stack, world, entity, seed) -> entity != null && CrossbowItem.isCharged(stack) && CrossbowItem.hasProjectile(stack, Items.FIREWORK_ROCKET) ? 1.0f : 0.0f);
        }
    }
}
